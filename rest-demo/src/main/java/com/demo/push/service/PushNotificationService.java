package com.demo.push.service;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.ResourceAccessException;

import com.demo.push.request.PushNotificationMessage;

public interface PushNotificationService {

	@Async
	@Retryable(value = { ResourceAccessException.class }, maxAttempts = 5, backoff = @Backoff(delay = 1000, multiplier = 2))
	public void sendPush(PushNotificationMessage message) ;

}
