package com.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.demo.model.PaymentTransaction;

public interface PaymentTransactionRepository extends CrudRepository<PaymentTransaction, Long> {

	List<PaymentTransaction> findByToAccountCustomerUserId(Long userId);
	
}
